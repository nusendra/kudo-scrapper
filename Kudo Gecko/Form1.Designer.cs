﻿namespace Kudo_Gecko
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.geckoWebBrowser1 = new Gecko.GeckoWebBrowser();
            this.label4 = new System.Windows.Forms.Label();
            this.txtsaldo = new System.Windows.Forms.TextBox();
            this.btnonoff = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtpin = new System.Windows.Forms.TextBox();
            this.lblstatus = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtport = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txthitung = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.btnlogout = new System.Windows.Forms.Button();
            this.btngagal = new System.Windows.Forms.Button();
            this.btnsukses = new System.Windows.Forms.Button();
            this.btnsiapbayar = new System.Windows.Forms.Button();
            this.btnagent = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.lblstatusweb = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.hitungerror = new System.Windows.Forms.TextBox();
            this.checkvoucher = new System.Windows.Forms.CheckBox();
            this.txtvoucher = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.timer3 = new System.Windows.Forms.Timer(this.components);
            this.timer4 = new System.Windows.Forms.Timer(this.components);
            this.timer5 = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // geckoWebBrowser1
            // 
            this.geckoWebBrowser1.FrameEventsPropagateToMainWindow = false;
            this.geckoWebBrowser1.Location = new System.Drawing.Point(9, 37);
            this.geckoWebBrowser1.Name = "geckoWebBrowser1";
            this.geckoWebBrowser1.Size = new System.Drawing.Size(357, 445);
            this.geckoWebBrowser1.TabIndex = 0;
            this.geckoWebBrowser1.UseHttpActivityObserver = false;
            this.geckoWebBrowser1.Click += new System.EventHandler(this.geckoWebBrowser1_Click);
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(432, 491);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 13);
            this.label4.TabIndex = 35;
            this.label4.Text = "Saldo";
            // 
            // txtsaldo
            // 
            this.txtsaldo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtsaldo.Location = new System.Drawing.Point(485, 488);
            this.txtsaldo.Name = "txtsaldo";
            this.txtsaldo.ReadOnly = true;
            this.txtsaldo.Size = new System.Drawing.Size(121, 20);
            this.txtsaldo.TabIndex = 34;
            // 
            // btnonoff
            // 
            this.btnonoff.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnonoff.Location = new System.Drawing.Point(531, 6);
            this.btnonoff.Name = "btnonoff";
            this.btnonoff.Size = new System.Drawing.Size(75, 23);
            this.btnonoff.TabIndex = 33;
            this.btnonoff.Text = "ON";
            this.btnonoff.UseVisualStyleBackColor = true;
            this.btnonoff.Click += new System.EventHandler(this.btnonoff_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(190, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(22, 13);
            this.label3.TabIndex = 31;
            this.label3.Text = "Pin";
            // 
            // txtpin
            // 
            this.txtpin.Location = new System.Drawing.Point(214, 9);
            this.txtpin.Name = "txtpin";
            this.txtpin.Size = new System.Drawing.Size(77, 20);
            this.txtpin.TabIndex = 30;
            // 
            // lblstatus
            // 
            this.lblstatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblstatus.AutoSize = true;
            this.lblstatus.ForeColor = System.Drawing.Color.Red;
            this.lblstatus.Location = new System.Drawing.Point(287, 491);
            this.lblstatus.Name = "lblstatus";
            this.lblstatus.Size = new System.Drawing.Size(106, 13);
            this.lblstatus.TabIndex = 29;
            this.lblstatus.Text = "Menunggu transaksi ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(26, 13);
            this.label1.TabIndex = 26;
            this.label1.Text = "Port";
            // 
            // txtport
            // 
            this.txtport.Location = new System.Drawing.Point(45, 8);
            this.txtport.Name = "txtport";
            this.txtport.Size = new System.Drawing.Size(53, 20);
            this.txtport.TabIndex = 25;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(104, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 28;
            this.label2.Text = "Count";
            // 
            // txthitung
            // 
            this.txthitung.Location = new System.Drawing.Point(142, 9);
            this.txthitung.Name = "txthitung";
            this.txthitung.Size = new System.Drawing.Size(42, 20);
            this.txthitung.TabIndex = 27;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button1.Location = new System.Drawing.Point(390, 514);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(94, 23);
            this.button1.TabIndex = 41;
            this.button1.Text = "TRX PROSES";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnlogout
            // 
            this.btnlogout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnlogout.Location = new System.Drawing.Point(512, 514);
            this.btnlogout.Name = "btnlogout";
            this.btnlogout.Size = new System.Drawing.Size(94, 23);
            this.btnlogout.TabIndex = 40;
            this.btnlogout.Text = "LOGOUT";
            this.btnlogout.UseVisualStyleBackColor = true;
            this.btnlogout.Click += new System.EventHandler(this.btnlogout_Click);
            // 
            // btngagal
            // 
            this.btngagal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btngagal.Location = new System.Drawing.Point(290, 514);
            this.btngagal.Name = "btngagal";
            this.btngagal.Size = new System.Drawing.Size(94, 23);
            this.btngagal.TabIndex = 39;
            this.btngagal.Text = "TRX GAGAL";
            this.btngagal.UseVisualStyleBackColor = true;
            this.btngagal.Click += new System.EventHandler(this.btngagal_Click);
            // 
            // btnsukses
            // 
            this.btnsukses.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnsukses.Location = new System.Drawing.Point(190, 514);
            this.btnsukses.Name = "btnsukses";
            this.btnsukses.Size = new System.Drawing.Size(94, 23);
            this.btnsukses.TabIndex = 38;
            this.btnsukses.Text = "TRX SUKSES";
            this.btnsukses.UseVisualStyleBackColor = true;
            this.btnsukses.Click += new System.EventHandler(this.btnsukses_Click);
            // 
            // btnsiapbayar
            // 
            this.btnsiapbayar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnsiapbayar.Location = new System.Drawing.Point(90, 514);
            this.btnsiapbayar.Name = "btnsiapbayar";
            this.btnsiapbayar.Size = new System.Drawing.Size(94, 23);
            this.btnsiapbayar.TabIndex = 37;
            this.btnsiapbayar.Text = "SIAP BAYAR";
            this.btnsiapbayar.UseVisualStyleBackColor = true;
            this.btnsiapbayar.Click += new System.EventHandler(this.btnsiapbayar_Click);
            // 
            // btnagent
            // 
            this.btnagent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnagent.Location = new System.Drawing.Point(9, 514);
            this.btnagent.Name = "btnagent";
            this.btnagent.Size = new System.Drawing.Size(75, 23);
            this.btnagent.TabIndex = 36;
            this.btnagent.Text = "AGENT";
            this.btnagent.UseVisualStyleBackColor = true;
            this.btnagent.Click += new System.EventHandler(this.btnagent_Click);
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 491);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 13);
            this.label5.TabIndex = 42;
            this.label5.Text = "Status Load:";
            // 
            // lblstatusweb
            // 
            this.lblstatusweb.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblstatusweb.AutoSize = true;
            this.lblstatusweb.Location = new System.Drawing.Point(79, 491);
            this.lblstatusweb.Name = "lblstatusweb";
            this.lblstatusweb.Size = new System.Drawing.Size(0, 13);
            this.lblstatusweb.TabIndex = 43;
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // richTextBox1
            // 
            this.richTextBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox1.Location = new System.Drawing.Point(376, 37);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(230, 445);
            this.richTextBox1.TabIndex = 32;
            this.richTextBox1.Text = "";
            this.richTextBox1.TextChanged += new System.EventHandler(this.richTextBox1_TextChanged);
            // 
            // timer2
            // 
            this.timer2.Interval = 1000;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // hitungerror
            // 
            this.hitungerror.Location = new System.Drawing.Point(398, 488);
            this.hitungerror.Name = "hitungerror";
            this.hitungerror.Size = new System.Drawing.Size(28, 20);
            this.hitungerror.TabIndex = 44;
            // 
            // checkvoucher
            // 
            this.checkvoucher.AutoSize = true;
            this.checkvoucher.Location = new System.Drawing.Point(390, 10);
            this.checkvoucher.Name = "checkvoucher";
            this.checkvoucher.Size = new System.Drawing.Size(29, 17);
            this.checkvoucher.TabIndex = 48;
            this.checkvoucher.Text = " ";
            this.checkvoucher.UseVisualStyleBackColor = true;
            // 
            // txtvoucher
            // 
            this.txtvoucher.Location = new System.Drawing.Point(407, 9);
            this.txtvoucher.Name = "txtvoucher";
            this.txtvoucher.Size = new System.Drawing.Size(118, 20);
            this.txtvoucher.TabIndex = 49;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(315, 11);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(69, 13);
            this.label6.TabIndex = 50;
            this.label6.Text = "Kode Voucer";
            // 
            // timer3
            // 
            this.timer3.Interval = 1000;
            this.timer3.Tick += new System.EventHandler(this.timer3_Tick);
            // 
            // timer4
            // 
            this.timer4.Interval = 1000;
            this.timer4.Tick += new System.EventHandler(this.timer4_Tick);
            // 
            // timer5
            // 
            this.timer5.Interval = 1000;
            this.timer5.Tick += new System.EventHandler(this.timer5_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(615, 542);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtvoucher);
            this.Controls.Add(this.checkvoucher);
            this.Controls.Add(this.hitungerror);
            this.Controls.Add(this.lblstatusweb);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnlogout);
            this.Controls.Add(this.btngagal);
            this.Controls.Add(this.btnsukses);
            this.Controls.Add(this.btnsiapbayar);
            this.Controls.Add(this.btnagent);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtsaldo);
            this.Controls.Add(this.btnonoff);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtpin);
            this.Controls.Add(this.lblstatus);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtport);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txthitung);
            this.Controls.Add(this.geckoWebBrowser1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Kudo Gecko Scraper Ver 2.5.1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Gecko.GeckoWebBrowser geckoWebBrowser1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtsaldo;
        private System.Windows.Forms.Button btnonoff;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtpin;
        private System.Windows.Forms.Label lblstatus;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtport;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txthitung;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnlogout;
        private System.Windows.Forms.Button btngagal;
        private System.Windows.Forms.Button btnsukses;
        private System.Windows.Forms.Button btnsiapbayar;
        private System.Windows.Forms.Button btnagent;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblstatusweb;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.TextBox hitungerror;
        private System.Windows.Forms.CheckBox checkvoucher;
        private System.Windows.Forms.TextBox txtvoucher;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Timer timer3;
        private System.Windows.Forms.Timer timer4;
        private System.Windows.Forms.Timer timer5;
    }
}

