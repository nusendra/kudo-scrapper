﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;
using Ini;
using System.Windows.Forms;

namespace Kudo_Gecko
{
    class database
    {
        public DataTable SQLCari(string query)
        {
            IniFile ini = new IniFile(Application.StartupPath + "\\setting.ini");
            string db = ini.IniReadValue("Info", "db");
            string ipdb = ini.IniReadValue("Info", "ipdb");

            string dns = "Server='" + ipdb + "';port=3306;user id='root';password='';database='" + db + "'";

            using (MySqlConnection koneksi = new MySqlConnection(dns))
            {
                using (MySqlDataAdapter da = new MySqlDataAdapter(query, koneksi))
                {
                    DataTable Dtb = new DataTable();
                    koneksi.Open();
                    da.Fill(Dtb);
                    return Dtb;
                }
            }
        }

        public string SQLQuery(string query)
        {
            IniFile ini = new IniFile(Application.StartupPath + "\\setting.ini");
            string db = ini.IniReadValue("Info", "db");
            string ipdb = ini.IniReadValue("Info", "ipdb");

            string dns = "Server='" + ipdb + "';port=3306;user id='root';password='';database='" + db + "'";

            using (MySqlConnection koneksi = new MySqlConnection(dns))
            {
                using (MySqlCommand sqlcmd = new MySqlCommand(query, koneksi))
                {
                    koneksi.Open();
                    sqlcmd.ExecuteNonQuery();
                    return "OK";
                }
            }
        }
    }
}
