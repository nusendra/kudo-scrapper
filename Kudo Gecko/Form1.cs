﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Gecko;
using MySql.Data.MySqlClient;
using Ini;
using System.Text.RegularExpressions;

namespace Kudo_Gecko
{
    public partial class Form1 : Form
    {
        database db = new database();
        DataTable dttb;
        String kartu;
        String kode;
        String nomor;
        String lanjut;
        String AdaDenom;
        String count;
        String tgl;
        String proses;
        String SN;
        String agent;
        int hitungvoucher = 0;
        int hitungklikbutton = 0;
        int hitungklikbuttonLanjut = 0;

        IniFile ini = new IniFile(Application.StartupPath + "\\setting.ini");

        public Form1()
        {
            InitializeComponent();
            this.Text = "Kudo Gecko Scraper Ver 2.5.1 | Account : " + ini.IniReadValue("Info", "account");

            var path = Application.StartupPath + "\\Firefox";
            Gecko.Xpcom.Initialize(path);

            string sUserAgent = ini.IniReadValue("Info", "identitas");
            Gecko.GeckoPreferences.User["general.useragent.override"] = sUserAgent;
        }

        private void WebLoading()
        {
            lblstatusweb.Text = "Web sedang dimuat.....";
            lblstatusweb.ForeColor = Color.DarkOrange;
        }

        private void WebFinish()
        {
            lblstatusweb.Text = "Web berhasil di muat.";
            lblstatusweb.ForeColor = Color.Green;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            tgl = DateTime.Now.ToString("yyyy-MM-dd");

            richTextBox1.Text += Environment.NewLine + "Kudo Mulai jalan";
            richTextBox1.Text += Environment.NewLine + "------------------------------------------------------";
            richTextBox1.SelectionStart = richTextBox1.Text.Length;
            richTextBox1.ScrollToCaret();

            geckoWebBrowser1.Navigate("https://kudo.co.id/shop/auth/login");
            //geckoWebBrowser1.Navigate("https://kudo.co.id/shop");

            txtpin.Text = ini.IniReadValue("Info", "pin");
            count = ini.IniReadValue("Info", "count");
            txtport.Text = ini.IniReadValue("Info", "port");
            proses = "CekSaldo";
            WebLoading();
            geckoWebBrowser1.DocumentCompleted += geckoWebBrowser1_DocumentCompleted;
        }

        private void geckoWebBrowser1_DocumentCompleted(object sender, Gecko.Events.GeckoDocumentCompletedEventArgs e)
        {
            if (e.Uri.Equals(geckoWebBrowser1.Url.ToString()))
            {
                switch (proses)
                {
                    case "default":
                        WebFinish();
                        break;
                    case "CekSaldo":
                        WebFinish();
                        Ceksaldo();
                        break;
                    case "PilihPulsaElektrik":
                        WebFinish();
                        PilihPulsaElektrik();
                        break;
                    case "InputNomor":
                        WebFinish();
                        InputNomor();
                        break;
                    //case "KlikDenomBeli":
                    //    WebFinish();
                    //    KlikDenomBeli();
                    //    break;
                    case "KlikBayar":
                        WebFinish();
                        KlikBayar();
                        break;
                    case "StatusTransaksi":
                        WebFinish();
                        StatusTransaksi();
                        break;
                    case "TransaksiSukses":
                        WebFinish();
                        TransaksiSukses();
                        break;
                }
            }
        }

        private void Ceksaldo()
        {
            String web = geckoWebBrowser1.Url.ToString();
            if (web == "https://kudo.co.id/shop/agent")
            {
                GeckoHtmlElement saldo = geckoWebBrowser1.Document.GetHtmlElementById("rekam_dana");
                txtsaldo.Text = Regex.Replace(saldo.TextContent, @"\s+", "");
            }
        }

        private void MulaiLagi()
        {
            txthitung.Text = "0";
            hitungerror.Text = "0";

            timer1.Enabled = true;
            timer2.Enabled = true;
            lblstatus.Text = "Menunggu Transaksi";
            lblstatus.ForeColor = Color.Red;
            richTextBox1.Focus();
        }

        private void CekOutbox()
        {
            dttb = db.SQLCari("select no_hp,nama,pesan,com from sms_outbox where com = '" + txtport.Text + "' order by id limit 1");
            if (dttb.Rows.Count > 0)
            {
                richTextBox1.Text += Environment.NewLine + " ";
                richTextBox1.Text += Environment.NewLine + "------------------------------------------------------";
                richTextBox1.SelectionStart = richTextBox1.Text.Length;
                richTextBox1.ScrollToCaret();
                //MessageBox.Show("b");
                timer1.Enabled = false;
                lblstatus.Text = "Transaksi Aktif";
                lblstatus.ForeColor = Color.Green;
                try
                {
                    //MessageBox.Show("c");
                    String[] pesan = dttb.Rows[0]["pesan"].ToString().Split('|');
                    kartu = pesan[0].ToLower();
                    kode = "Voucher Rp" + pesan[1];
                    nomor = pesan[2];
                    richTextBox1.Text += Environment.NewLine + "WAKTU : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss");
                    richTextBox1.Text += Environment.NewLine + "OPERATOR : " + kartu;
                    richTextBox1.Text += Environment.NewLine + "DENOM : " + kode;
                    richTextBox1.Text += Environment.NewLine + "NOMOR TUJUAN : " + nomor;
                    richTextBox1.SelectionStart = richTextBox1.Text.Length;
                    richTextBox1.ScrollToCaret();

                    //geckoWebBrowser1.Navigate("https://kudo.co.id/shop/pulsa/" + kartu + "/?no=" + nomor + "&type=1");
                    geckoWebBrowser1.Navigate("https://kudo.co.id/shop");
                    db.SQLQuery("delete from sms_outbox where com='" + txtport.Text + "' order by id limit 1");

                    //proses = "KlikDenomBeli";
                    proses = "PilihPulsaElektrik";
                    WebLoading();
                    //geckoWebBrowser1.DocumentCompleted += geckoWebBrowser1_DocumentCompleted;
                }
                catch (Exception ex)
                {

                }

            }
        }

        private void PilihPulsaElektrik()
        {
            var njing = new Gecko.DOM.GeckoInputElement(geckoWebBrowser1.Document.GetElementById("pulsa-dan-paket-data").DomObject);
            njing.Click();
            
            proses = "InputNomor";
            WebLoading();
        }

        private void InputNomor()
        {
            geckoWebBrowser1.Focus();
            var setNomor = new Gecko.DOM.GeckoInputElement(geckoWebBrowser1.Document.GetElementById("idPhoneNumber").DomObject);
            setNomor.Focus();
            setNomor.Value = nomor;
            SendKeys.Send("{DIVIDE}");
            
            timer4.Enabled = true;
        }

        private void pilihDenom()
        {
            var divs = geckoWebBrowser1.Document.GetElementsByClassName("list-pulsa-left-1");
            foreach (GeckoHtmlElement div in divs)
            {
                if (div.TextContent == kode)
                {
                    AdaDenom = "ada";
                    div.Click();
                    break;
                }
            }
            timer5.Enabled = true;
        }

        private void klikButtonLanjut()
        {
            if (AdaDenom == "ada")
            {
                GeckoHtmlElement btn = geckoWebBrowser1.Document.GetElementsByTagName("button")[0];
                btn.Click();
                richTextBox1.Focus();
                proses = "KlikBayar";
                WebLoading();
            }
            else
            {
                richTextBox1.Text += Environment.NewLine + " ";
                richTextBox1.Text += Environment.NewLine + ".:: PRODUK CLOSE ::.";
                richTextBox1.SelectionStart = richTextBox1.Text.Length;
                richTextBox1.ScrollToCaret();
                db.SQLQuery("insert into sms_inbox(sourceno,nama,tgl,text,type,port,ip,iscentre) select 'kudo',nama,now(),'" + nomor + " gagal',13,'200','192.168.2.3',1 from distributor where replyno='kudo'");
                proses = "CekSaldo";
                MulaiLagi();
            }
        }
        
        private void KlikBayar()
        {
            if (checkvoucher.Checked == true)
            {
                var njing = new Gecko.DOM.GeckoInputElement(geckoWebBrowser1.Document.GetElementById("discount-check").DomObject);
                njing.Click();

                Gecko.DOM.GeckoInputElement su = new Gecko.DOM.GeckoInputElement(geckoWebBrowser1.Document.GetElementsByName("voucher")[0].DomObject);
                su.Focus();
                su.Value = txtvoucher.Text;

                geckoWebBrowser1.Navigate("javascript:onCekVoucher()");

                timer3.Enabled = true;
            }else
            {
                geckoWebBrowser1.Navigate("javascript:setToPay(); trackCheckoutPay('confirm-checkout')");

                try
                {
                    var setpin = new Gecko.DOM.GeckoInputElement(geckoWebBrowser1.Document.GetElementById("check").DomObject);
                    setpin.Focus();
                    setpin.Value = txtpin.Text;

                    geckoWebBrowser1.Navigate("javascript:doOrder(); ga_tracker('transaction', 'PROCESS_PURCHASE');");

                    proses = "StatusTransaksi";
                    WebLoading();
                }
                catch (Exception ex)
                {

                }
            }
            //geckoWebBrowser1.DocumentCompleted += geckoWebBrowser1_DocumentCompleted;
        }

        private void StatusTransaksi()
        {
            var links = geckoWebBrowser1.Document.GetElementsByClassName("dt-order-id font-bold primary-green");

            foreach (GeckoHtmlElement link in links)
            {
                SN = link.TextContent;
                break;
            }

            proses = "TransaksiSukses";
            geckoWebBrowser1.Navigate("https://kudo.co.id/shop/agent/");
        }

        private void TransaksiSukses()
        {
            String web = geckoWebBrowser1.Url.ToString();
            if (web == "https://kudo.co.id/shop/agent/")
            {
                var links = geckoWebBrowser1.Document.GetElementsByClassName("profile-data-name font-bold");

                foreach (GeckoHtmlElement link in links)
                {
                    agent = link.TextContent;
                    break;
                }

                var saldos = geckoWebBrowser1.Document.GetElementsByClassName("pos-abs full item-name");

                foreach (GeckoHtmlElement saldo in saldos)
                {
                    txtsaldo.Text = Regex.Replace(saldo.TextContent, @"\s+", "");
                    break;
                }
            }

            //proses = "GetSaldo";
            //geckoWebBrowser1.Navigate("https://kudo.co.id/shop/");
            richTextBox1.Text += Environment.NewLine + " ";
            richTextBox1.Text += Environment.NewLine + nomor + " SUKSES SN : " + SN;
            richTextBox1.SelectionStart = richTextBox1.Text.Length;
            richTextBox1.ScrollToCaret();
            db.SQLQuery("insert into sms_inbox(sourceno,nama,tgl,text,type,port,ip,iscentre) select 'kudo',nama,now(),'" + nomor + " sukses dgn SN=" + SN + ". User Agent = " + agent + ". Sisa saldo " + txtsaldo.Text + "', 13,'" + txtport.Text + "','192.168.2.3',1 from distributor where replyno='kudo'");
            MulaiLagi();

        }

        private void geckoWebBrowser1_Click(object sender, EventArgs e)
        {

        }

        private void btnsiapbayar_Click(object sender, EventArgs e)
        {
            proses = "default";
            WebLoading();
            geckoWebBrowser1.Navigate("https://kudo.co.id/shop/agent/transaksi-aktif");
            //geckoWebBrowser1.DocumentCompleted += geckoWebBrowser1_DocumentCompleted;
        }
        
        private void btnagent_Click(object sender, EventArgs e)
        {
            proses = "default";
            WebLoading();
            geckoWebBrowser1.Navigate("https://kudo.co.id/shop/agent");
            //geckoWebBrowser1.DocumentCompleted += geckoWebBrowser1_DocumentCompleted;
        }

        private void btnsukses_Click(object sender, EventArgs e)
        {
            proses = "default";
            WebLoading();
            geckoWebBrowser1.Navigate("https://kudo.co.id/shop/agent/rekam-transaksi?date_to=" + tgl + "&date_from=" + tgl + "&status=3");
            //geckoWebBrowser1.DocumentCompleted += geckoWebBrowser1_DocumentCompleted;
        }

        private void btngagal_Click(object sender, EventArgs e)
        {
            proses = "default";
            WebLoading();
            geckoWebBrowser1.Navigate("https://kudo.co.id/shop/agent/rekam-transaksi?date_to=" + tgl + "&date_from=" + tgl + "&status=9");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            proses = "default";
        }

        private void btnlogout_Click(object sender, EventArgs e)
        {
            proses = "default";
            WebLoading();
            geckoWebBrowser1.Navigate("https://kudo.co.id/shop/agent/setting");
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            int hitung = 0;
            hitung++;
            txthitung.Text = (Convert.ToInt32(txthitung.Text) + hitung).ToString();
            if (txthitung.Text == count)
            {
                CekOutbox();
                txthitung.Text = "0";

            }
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {
            var linecount = richTextBox1.Lines.Count();
            if (linecount == 100)
            {
                richTextBox1.Clear();
            }
        }

        private void btnonoff_Click(object sender, EventArgs e)
        {
            if (btnonoff.Text == "ON")
            {
                MulaiLagi();
                btnonoff.Text = "OFF";
            }
            else
            {
                btnonoff.Text = "ON";
                timer1.Enabled = false;
                timer2.Enabled = false;
                lblstatus.Text = "Menunggu Transaksi";
                lblstatus.ForeColor = Color.Red;
            }
            geckoWebBrowser1.Focus();
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            int hitung = 0;
            hitung++;
            hitungerror.Text = (Convert.ToInt32(hitungerror.Text) + hitung).ToString();
            if (hitungerror.Text == "10")
            {
                try
                {
                    var links = geckoWebBrowser1.Document.GetElementsByClassName("konfimasi-message ptop10 pbot10");

                    foreach (GeckoHtmlElement link in links)
                    {
                        if (AdaDenom == "ada")
                        {
                            geckoWebBrowser1.Navigate("https://kudo.co.id/shop");
                            proses = "PilihPulsaElektrik";
                            WebLoading();
                            lblstatusweb.Text = "Kudo Error";
                            lblstatusweb.ForeColor = Color.Red;
                            richTextBox1.Text += Environment.NewLine + " ";
                            richTextBox1.Text += Environment.NewLine + "Kudo error, ulang otomatis";
                            richTextBox1.Text += Environment.NewLine + " ";
                            richTextBox1.SelectionStart = richTextBox1.Text.Length;
                            richTextBox1.ScrollToCaret();
                            //kudoerror = "ya";
                            break;
                        }

                    }
                }
                catch (Exception ex)
                {

                }
                hitungerror.Text = "0";

            }
        }

        private void timer3_Tick(object sender, EventArgs e)
        {
            
            hitungvoucher++;
            if (hitungvoucher == 10)
            {
                geckoWebBrowser1.Navigate("javascript:setToPay(); trackCheckoutPay('confirm-checkout')");

                try
                {
                    var setpin = new Gecko.DOM.GeckoInputElement(geckoWebBrowser1.Document.GetElementById("check").DomObject);
                    setpin.Focus();
                    setpin.Value = txtpin.Text;

                    geckoWebBrowser1.Navigate("javascript:doOrder(); ga_tracker('transaction', 'PROCESS_PURCHASE');");

                    proses = "StatusTransaksi";
                    WebLoading();
                }
                catch (Exception ex)
                {

                }
                hitungvoucher = 0;
                timer3.Enabled = false;
            }
        }

        private void timer4_Tick(object sender, EventArgs e)
        {
            hitungklikbutton++;
            //richTextBox1.Text += Environment.NewLine + " a - " + hitungklikbutton;
            if (hitungklikbutton == 2)
            {
                pilihDenom();
                hitungklikbutton = 0;
                timer4.Enabled = false;
            }
        }

        private void timer5_Tick(object sender, EventArgs e)
        {
            hitungklikbuttonLanjut++;
            //richTextBox1.Text += Environment.NewLine + " b - " + hitungklikbuttonLanjut;
            if (hitungklikbuttonLanjut == 2)
            {
                klikButtonLanjut();
                hitungklikbuttonLanjut = 0;
                timer5.Enabled = false;
            }
        }
    }
}
